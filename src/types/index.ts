export type User = {
    id: string;
    username: string;
    login: string;
}

export type Post = {
    id: string;
    name: string;
    text: string;
    link: string;
    userId: string;
}

export type DefaultReject = {
    message: string;
    error: string;
    statusCode: number;
};
