import { routesNames } from 'constant';
import { Auth, Main, Post } from 'pages';
import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from 'store';
import "./App.css"

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path={routesNames.auth} element={<Auth />} />
          <Route path={routesNames.main} element={<Main />} />
          <Route path={routesNames.post} element={<Post />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
