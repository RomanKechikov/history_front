import axios from 'axios';
import store, { actions } from 'store';

export const API_URL = 'https://historyback.herokuapp.com/';

const api = axios.create({
  baseURL: API_URL,
});

export default api;

api.interceptors.request.use((config) => {
  const token = store.getState().user.jwtToken;

  if (!token) {
    return config;
  }

  const Authorization = `Bearer ${token}`;

  return {
    ...config,
    headers: { ...config.headers, Authorization },
  };
});

api.interceptors.response.use(
  (response) => {
    if (response.status === 401) {
      store.dispatch(actions.user.logout());
    } else if (response.status >= 400) {
      throw new Error(response.data);
    }
    return response;
  },
  (error) => {
    if (error.response.status && error.response.status === 401) {
      store.dispatch(actions.user.logout());
    }

    throw error.response.data;
  }
);
