import { useAppDispatch } from "hooks";
import React, { useState } from "react";
import { actions } from "store";
import "./modal.css"

type Props = {
  setIsOpen: (isOpen: boolean) => void;
  isOpen: boolean;
}

const Modal = ({ isOpen, setIsOpen }: Props) => {
  const dispatch = useAppDispatch()
  const [name, setName] = useState('')
  const [link, setLink] = useState('')
  const [text, setText] = useState('')

  const handleSubmit = async (e:React.FormEvent<HTMLFormElement>) => { 
    e.preventDefault()
    const data = {name, link, text}
    const {meta} = await dispatch(actions.post.createPost(data))
    if (meta.requestStatus === 'fulfilled') {
      dispatch(actions.post.getAll())
      setIsOpen(false);
    }
  }
  return isOpen
    ?
    <div className="modal-wrapper"
      onClick={() => setIsOpen(false)}
    >
      <form className="modal"
        onClick={event => event.stopPropagation()}
        onSubmit={e => handleSubmit(e)}
      >
        <button onClick={() => setIsOpen(false)} className="modal__btn-close" type="button">&#10006;</button>
        <div className="modal-content">
          <p className="modal__title">
            Добавить пост
          </p>
          <p className="modal__support-text">
            Название
          </p>
          <input className='modal__input' value={name} onChange={e => setName(e.target.value)} required />
          <p className="modal__support-text">
            Ссылка на картинку
          </p>
          <input className='modal__input' value={link} onChange={e => setLink(e.target.value)} required />
          <p className="modal__support-text">
            Текст поста
          </p>
          <textarea className='modal__text-body' value={text} onChange={e => setText(e.target.value)} required/>

        </div>
        <button className='modal__make-post' type="submit">
        Создать пост
      </button>
      </form>
    </div>
    :
    <></>
}

export default Modal;