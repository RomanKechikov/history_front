import { routesNames } from 'constant'
import { useAppDispatch } from 'hooks'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { actions } from 'store'
import "./signup.css"

type Props = {setisSignUp: (value: boolean) => void}

const SignUp = (props: Props) => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const [username, setUsername] = useState('')
  const [login, setLogin] = useState('')
  const [password, setPassword] = useState('')

  const handleSubmit = async (e:React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const data = {username, login, password}
    const {meta} = await dispatch(actions.user.signUp(data))
    if (meta.requestStatus === 'fulfilled') {
      navigate(routesNames.main);
    }
  }

  return (
    
    <form className='signup'
    onSubmit={e => handleSubmit(e)}>
      <p className="signup__title">
        Регистрация
      </p>
      <p className="signup__support-text">
        Имя
      </p>
      <input className='signup__input' value={username} onChange={e => setUsername(e.target.value)} required/>
      <p className="signup__support-text">
        Логин
      </p>
      <input className='signup__input' value={login} onChange={e => setLogin(e.target.value)} required />
      <p className="signup__support-text">
        Пароль
      </p>
      <input className='signup__input' type='password' value={password} onChange={e => setPassword(e.target.value)} required />
      <button className='signup__btn' type='submit'>
        Регистрация
      </button>
      <button className='signup__btn' onClick={() => props.setisSignUp(true)} type='button'>
        Войти
      </button>
    </form>
  )
}

export default SignUp