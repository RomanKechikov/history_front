import Modal from 'components/modal'
import { useAppSelector } from 'hooks'
import React, { useState } from 'react'
import { actions, selectors } from 'store'
import { useAppDispatch } from 'hooks'
import { useNavigate } from 'react-router-dom'
import "./header.css"
import { routesNames } from 'constant'

type Props = {}

const Header = (props: Props) => {

  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const handleLogout = () => {
    dispatch(actions.user.logout())
    navigate(routesNames.auth)
  }

  const isAuth = useAppSelector(selectors.user.selectIsAuth)
  const [isOpen, setIsOpen] = useState(false)
  const username = useAppSelector(selectors.user.selectUsername)
  const renderNavigate = () => {
    if (isAuth) {
      return (
        <>
          <button className='header-navigate__btn' onClick={() => setIsOpen(true)}>
            Добавить пост
          </button>
          <Modal isOpen={isOpen} setIsOpen={setIsOpen}/>
          <p className=''> {username} </p>
          <button className='header-navigate__btn' onClick={handleLogout}>
            <img src="./img/logout.png" alt="Выйти" className='header-navigate__btn-logout-img' />
          </button>
        </>
      )
    }
    return <button className='header-navigate__btn-login' onClick={() => navigate(routesNames.auth)}> Войти </button>
  }

  return (
    <div className='header'>
      <div className='header-logo' onClick={ () => navigate(routesNames.main)}>
        <img src="./img/logo.jpg" alt="logo" className='header-logo__img' />
        <p className='header-logo__title'>
          История
        </p>
      </div>

      <p className='header__subtitle'>
        Проект о главных сражениях в истории России
      </p>
      <div className='header-navigate'>
        { renderNavigate() }
      </div>
      
    </div>
  )
}

export default Header