import post from 'pages/post'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Post } from 'types'
import "./cardPreview.css"

type Props = {post: Post}

const CardPreview = (props: Props) => {
  const navigate = useNavigate()
  return (
    <div className='main-cardPreview' onClick={() => navigate(`/${props.post.id}`)}>
      <img src={props.post.link} alt="" className='main-cardPreview__img'/>
      <h2 className='main-cardPreview__title'>
        {props.post.name}
      </h2>
    </div>
  )
}

export default CardPreview