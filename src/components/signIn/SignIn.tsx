import { routesNames } from 'constant'
import { useAppDispatch } from 'hooks'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { actions } from 'store'
import "./signin.css"

type Props = {setisSignUp: (value: boolean) => void}

const SignIn = (props: Props) => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const [login, setLogin] = useState('')
  const [password, setPassword] = useState('')

  const handleSubmit = async (e:React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const data = {login, password}
    const {meta} = await dispatch(actions.user.signIn(data))
    if (meta.requestStatus === 'fulfilled') {
      navigate(routesNames.main);
    }
  }
  return (
    
    <form className='signin'
    onSubmit={e => handleSubmit(e)}>
      <p className="signin__title">
        Авторизация
      </p>
      <p className="signin__support-text">
        Логин
      </p>
      <input className='signin__input' value={login} onChange={e => setLogin(e.target.value)} required />
      <p className="signin__support-text">
        Пароль
      </p>
      <input className='signin__input' type='password' value={password} onChange={e => setPassword(e.target.value)} required />
      <button className='signin__btn' type='submit' >
        Войти
      </button>
      <button className='signin__btn' onClick={() => props.setisSignUp(false)} type='button'>
        Регистрация
      </button>
    </form>
  )
}

export default SignIn
