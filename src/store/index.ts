import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './ducks'

const store = configureStore({ reducer: rootReducer })

export default store;

export { actions, selectors } from './ducks';

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
