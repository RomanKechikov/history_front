import { combineReducers } from '@reduxjs/toolkit';
import { default as userSlice, userActions, userSelectors } from './user';
import { default as postSlice, postActions, postSelectors } from './post';


const rootReducer = combineReducers({
  user: userSlice,
  post: postSlice
});

export default rootReducer;

export const actions = {
  user: userActions,
  post: postActions
};

export const selectors = {
  user: userSelectors,
  post: postSelectors
};
