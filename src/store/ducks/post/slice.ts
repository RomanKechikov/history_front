import { createSlice, isAnyOf, PayloadAction } from '@reduxjs/toolkit';
import { Post } from 'types';
import { getAll, createPost, deletePost } from './actions';

type InitialState = {
    posts: Post[]
}

const postSlice = createSlice({
    name: 'post',
    initialState: {

    } as InitialState,
    reducers: {

    },
    extraReducers: (builder) => {
        builder
            .addCase(getAll.fulfilled, (state, { payload }) => {
                state.posts = payload
            })
    },
})

export const postActions = { ...postSlice.actions, getAll, createPost, deletePost }

export default postSlice.reducer
