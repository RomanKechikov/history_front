import { RootState } from 'store';

export const selectPosts = (state: RootState) => state.post.posts;
export const selectPostById = (id?: string) => (state: RootState) => state.post.posts.find(post => post.id === id);