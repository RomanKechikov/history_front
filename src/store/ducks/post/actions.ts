import { createAsyncThunk } from "@reduxjs/toolkit";
import api from "services/http";
import { DefaultReject, Post } from "types";

export const getAll = createAsyncThunk<
    Post[],
    undefined,
    { rejectValue: DefaultReject }
>('getAllposts', async (_, { rejectWithValue }) => {
    try {
        const { data } = await api.get('posts');
        return data;
    } catch (error) {
        return rejectWithValue(error as DefaultReject);
    }
});

export const createPost = createAsyncThunk<
    Post,
    { name: string, text: string, link: string },
    { rejectValue: DefaultReject }
>('createPost', async (requestData, { rejectWithValue }) => {
    try {
        const { data } = await api.post('posts', requestData);
        return data;
    } catch (error) {
        return rejectWithValue(error as DefaultReject);
    }

    
});

export const deletePost = createAsyncThunk<
    boolean,
    { id: string},
    { rejectValue: DefaultReject }
>('deletePost', async (requestData, { rejectWithValue }) => {
    try {
        const { data } = await api.delete('posts/'+ requestData.id);
        return data;
    } catch (error) {
        return rejectWithValue(error as DefaultReject);
    }
});
