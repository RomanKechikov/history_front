import { createAsyncThunk } from "@reduxjs/toolkit";
import api from "services/http";
import { DefaultReject, User } from "types";

export const signUp = createAsyncThunk<
    { jwtToken: string, user: User },
    { login: string, password: string, username: string },
    { rejectValue: DefaultReject }
>('user/signUp', async (requestData, { rejectWithValue }) => {
    try {
        const { data } = await api.post('users/signUp', requestData);
        return data;
    } catch (error) {
        return rejectWithValue(error as DefaultReject);
    }
});

export const signIn = createAsyncThunk<
    { jwtToken: string, user: User },
    { login: string, password: string },
    { rejectValue: DefaultReject }
>('user/signIn', async (requestData, { rejectWithValue }) => {
    try {
        const { data } = await api.post('users/signIn', requestData);
        return data;
    } catch (error) {
        return rejectWithValue(error as DefaultReject);
    }
});
