import { createSlice, isAnyOf, PayloadAction } from '@reduxjs/toolkit';
import { User } from 'types';
import { signIn, signUp } from './actions';

type InitialState = {
    user: User | null,
    jwtToken: string | null
}

const userSlice = createSlice({
    name: 'user',
    initialState: {
        jwtToken: localStorage.getItem('token'),
        user: JSON.parse(localStorage.getItem('user') || '{}')
    } as InitialState,
    reducers: {
        logout: (state) => {
            state.jwtToken = null;
            state.user = null;
            localStorage.removeItem('token')
            localStorage.removeItem('user')
        },
    },
    extraReducers: (builder) => {
        builder
            .addMatcher(
                isAnyOf(signUp.fulfilled, signIn.fulfilled),
                (state, { payload }) => {                   
                    state.jwtToken = payload.jwtToken;
                    state.user = payload.user;
                    localStorage.setItem('token', state.jwtToken);
                    localStorage.setItem('user', JSON.stringify(state.user));
                }
            )
    },
})

export const userActions = { ...userSlice.actions, signIn, signUp }

export default userSlice.reducer
