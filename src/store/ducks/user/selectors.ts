import { RootState } from 'store';

export const selectUsername = (state: RootState) => state.user.user?.username;
export const selectIsAuth = (state: RootState) => !!state.user.jwtToken;
export const selectUserId = (state: RootState) => state.user.user?.id;