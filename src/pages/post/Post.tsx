import { Header } from 'components'
import { routesNames } from 'constant'
import { useAppDispatch, useAppSelector } from 'hooks'
import React from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { actions, selectors } from 'store'
import "./post.css"

type Props = {}

const Post = (props: Props) => {
  const navigate = useNavigate();
  const { id } = useParams();
  const isAuth = useAppSelector(selectors.user.selectIsAuth);
  const dispatch = useAppDispatch();
  const userId = useAppSelector(selectors.user.selectUserId);

  const post = useAppSelector(selectors.post.selectPostById(id))
  if (!post){
    navigate(routesNames.main);
    return null;
  }
  const handleDelete = async () => {
    if(!id) {
      return null
    }
    const {meta} = await dispatch(actions.post.deletePost({id}))
    if (meta.requestStatus === 'fulfilled') {
      dispatch(actions.post.getAll())
      navigate(routesNames.main)
    }
  }
  const renderDeletebtn = () => {
    if (isAuth && userId === post.userId) {
      return (
          <button className='main-card__delete-btn' onClick={handleDelete}>
            Удалить пост
          </button>
      )
    }
  }

  return (
    <div className='main-page'>
      <Header />
      <main className='main'>
        <div className="main-card">
          <img src={post?.link} alt="Картинка сражения" className='main-card__img' />
          <h2 className='main-card__title'>
            {post?.name}
          </h2>
          <p className='main-card__text'>
            {post?.text}
          </p>
          { renderDeletebtn() }
        </div>
      </main>
    </div>
  )
}

export default Post