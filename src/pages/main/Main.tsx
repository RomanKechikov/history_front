import { Header, CardPreview } from 'components'
import { useAppDispatch, useAppSelector } from 'hooks'
import React, { useEffect } from 'react'
import { actions, selectors } from 'store'
import post from 'store/ducks/post'
import "./main.css"

type Props = {}

const Main = (props: Props) => {
  const dispatch = useAppDispatch()
  const posts = useAppSelector(selectors.post.selectPosts)
  useEffect(() => { dispatch(actions.post.getAll()) }, [])

  const renderCards = () => {
    if(!posts){
      return null
    }
    return posts.map(post => <CardPreview post={post} key={post.id} />)
  }

  return (
    <div className='main-page'>
      <Header/>
      <main className='main'>
        { renderCards() }
      </main>
    </div>
  )
}

export default Main