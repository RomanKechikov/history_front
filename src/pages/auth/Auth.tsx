import { SignIn, SignUp } from 'components'
import React, { useState } from 'react'
import "./auth.css"

type Props = {}

const Auth = (props: Props) => {
  const [isSignUp, setisSignUp] = useState(true)
  return (
    <div className='auth-page'>

      <div className='auth-img' />
      <main className='auth-main'>
        {!isSignUp ? <SignIn setisSignUp={setisSignUp} /> : <SignUp setisSignUp={setisSignUp} />}
      </main>

    </div>
  )
}

export default Auth
